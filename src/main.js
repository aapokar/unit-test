const express = require('express')
const mylib = require('./mylib')
const port = 3000
const app = express()

console.log({
        sum: mylib.sum(1,2),
        random: mylib.random(),
        array: mylib.arrayGen()
    })

app.get('/add', (req, res) => {
    const a = parseInt(req.query.a)
    console.log(a)
    const b = parseInt(req.query.b)
    res.send(`${mylib.sum(a,b)}`)
})

app.get('/', (req, res) => {
    res.send('Hello world')
})

app.listen(port, () => {
    console.log(`Server running in port ${port}`)
})