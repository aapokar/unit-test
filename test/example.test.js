const {
    assert,
    should,
    expect
} = require('chai')
const mylib = require('../src/mylib')

describe('Unit testing mylib.js', () => {

    let myvar = undefined

    before(() => {
        myvar = 1
        console.log('func before testing')
    })
    after(() => {
        console.log('func after testing')
    })

    it('Myvar should exist', () => {
        should().exist(myvar)
    })

    it('Should return 2 when using sum function with a=1, b=1', () => {
        const result = mylib.sum(1, 1)
        expect(result).to.equal(2)
    })
    
    it('arrayGen() returns [1,2,3]', () => {
        const result = mylib.arrayGen()
        expect(result).to.deep.equal([1,2,3])
    })
    it('random() returns number', () => {
        const result = mylib.random()
        expect(result).to.be.an('number')
    })

    it('Assert foo is not bar', () => {
        assert('foo' !== 'bar')
    })
})